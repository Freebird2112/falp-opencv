import cv2
import numpy as np

# Leer la imagen original
img = cv2.imread('imagenes/23.jpg')

# Reajustar el tamaño de la imagen
resized_image = cv2.resize(img, (900, 900))

# Obtener el tamaño reajustado de la imagen
resized_height, resized_width, _ = resized_image.shape

# Definir el tamaño máximo del círculo como la mitad del menor valor entre resized_width y resized_height
max_circle_radius = min(resized_width, resized_height) // 2

# Generar un radio aleatorio para el círculo (entre 1 y max_circle_radius)
circle_radius = np.random.randint(1, max_circle_radius)

# Generar coordenadas aleatorias para el centro del círculo
center_x = np.random.randint(circle_radius, resized_width - circle_radius)
center_y = np.random.randint(circle_radius, resized_height - circle_radius)

# Dibujar el círculo en la imagen reajustada
cv2.circle(resized_image, (center_x, center_y), circle_radius, (0, 0, 255), 2)

# Convertir la imagen a escala de grises
gray = cv2.cvtColor(resized_image, cv2.COLOR_BGR2GRAY)


# Crear una copia de la imagen para el conteo de células blancas
white_cell_count_image = resized_image.copy()

# Crear una copia de la imagen para el conteo de células azules
blue_cell_count_image = resized_image.copy()



#####################################
## CONTEO CÉLULAS BLANCAS

# Aplicar un umbral para obtener una imagen binaria
_, thresh = cv2.threshold(gray, 128, 255, cv2.THRESH_BINARY)

# Aplicar un suavizado a la máscara para reducir el ruido
blurred_mask = cv2.GaussianBlur(thresh, (5, 5), 0)

# Encontrar los contornos en la imagen binaria
contours, _ = cv2.findContours(blurred_mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

# Inicializar el contador de células dentro del círculo
white_cell_count = 0

# Definir el área mínima para considerar un contorno como célula
min_cell_area_white = 50  # Ajusta este valor según tus necesidades

# Recorrer cada contorno encontrado
for contour in contours:
    
    # Calcular el área del contorno actual
    area = cv2.contourArea(contour)

    # Calcular el centro del contorno
    M = cv2.moments(contour)
    if M["m00"] != 0:
        cX = int(M["m10"] / M["m00"])
        cY = int(M["m01"] / M["m00"])
        
        # Calcular la distancia entre el centro del círculo y el centro del contorno
        distance = np.sqrt((cX - center_x) ** 2 + (cY - center_y) ** 2)
        
        # Verificar si el centro del contorno está dentro del círculo
        if distance <= circle_radius and area >= min_cell_area_white:
            # Incrementar el contador de células
            white_cell_count += 1
            
            # Dibujar el contorno en la imagen reajustada
            cv2.drawContours(white_cell_count_image, [contour], -1, (0, 255, 0), 1)
            
            # Mostrar el número del contorno en la imagen reajustada
            cv2.putText(white_cell_count_image, str(white_cell_count), (cX, cY), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 1)

# Mostrar el contador de células en la consola
print(f"El numero de celulas dentro del circulo es: {white_cell_count}")
############################################



'''
#CREACION MÁSCARA COLORES AZULES

# Convertir la imagen a HSV
hsv_image = cv2.cvtColor(blue_cell_count_image, cv2.COLOR_BGR2HSV)

# Defino el rango de colores azules o celestes en HSV
lower_blue = np.array([90, 50, 50])  # Rango inferior
upper_blue = np.array([130, 255, 255])  # Rango superior

# Crear una máscara para resaltar los colores azules o celestes en la imagen reajustada
blue_mask = cv2.inRange(hsv_image, lower_blue, upper_blue)

# Aplicar la máscara a la imagen original reajustada
blue_highlighted_image = cv2.bitwise_and(blue_cell_count_image, blue_cell_count_image, mask=blue_mask)
'''



############################################
## CONTEO CÉLULAS AZULES

# Convertir la imagen a HSV
hsv_image = cv2.cvtColor(blue_cell_count_image, cv2.COLOR_BGR2HSV)

# Definir el rango de colores azules o celestes en HSV
lower_blue = np.array([80, 50, 50])  # Rango inferior (azul)
upper_blue = np.array([160, 255, 255])  # Rango superior (morado)

# Crear una máscara para resaltar los colores azules o celestes
blue_mask = cv2.inRange(hsv_image, lower_blue, upper_blue)

# Aplicar un suavizado a la máscara para reducir el ruido
blurred_blue_mask = cv2.GaussianBlur(blue_mask, (5, 5), 0)

# Encontrar los contornos en la máscara de color azul suavizada
blue_contours, _ = cv2.findContours(blurred_blue_mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

# Definir el área mínima para considerar un contorno como una célula azul (ajusta este valor según tus necesidades)
min_blue_cell_area = 5  # Aumenta o disminuye este valor

# Inicializar el contador de células azules dentro del círculo
blue_cell_count = 0

# Recorrer cada contorno encontrado en la máscara de color azul
for blue_contour in blue_contours:
    #Calculo del área del contorno actual:
    area_blue = cv2.contourArea(blue_contour)


    # Calcular el centro del contorno
    M = cv2.moments(blue_contour)
    if M["m00"] != 0:
        cX = int(M["m10"] / M["m00"])
        cY = int(M["m01"] / M["m00"])
        
        
        # Calcular la distancia entre el centro del círculo y el centro del contorno
        distance = np.sqrt((cX - center_x) ** 2 + (cY - center_y) ** 2)
        
        # Verificar si el centro del contorno está dentro del círculo
        if distance <= circle_radius and area_blue >= min_blue_cell_area:
            # Incrementar el contador de células azules
            blue_cell_count += 1
            
            # Dibujar el contorno en la imagen reajustada
            cv2.drawContours(blue_cell_count_image, [blue_contour], -1, (255, 0, 0), 1)
            
            # Mostrar el número del contorno en la imagen reajustada
            cv2.putText(blue_cell_count_image, str(blue_cell_count), (cX, cY), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 1)

# Mostrar el contador de células azules en la consola
print(f"El numero de celulas azules dentro del circulo es: {blue_cell_count}")
############################################

# Cálculo del índice de proliferación
proliferation_index = (white_cell_count / (white_cell_count + blue_cell_count))*100
# Formatear el índice de proliferación con un decimal
formatted_proliferation_index = f"{proliferation_index:.1f}"
# Imprimir el índice de proliferación formateado
print(f"El indice de proliferacion es: {formatted_proliferation_index}")


# Mostrar la imagen reajustada con el rectángulo y los contornos
cv2.imshow('Resized Image with Rectangle and Contours', white_cell_count_image)
cv2.imshow('Blue cells', blue_cell_count_image)
cv2.waitKey(0)
cv2.destroyAllWindows()