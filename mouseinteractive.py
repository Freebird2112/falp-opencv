##PROGRAMA PARA ENCERRAR UN ÁREA MANUALMENTE
import cv2
import numpy as np

points = []  # Lista para almacenar las coordenadas de los puntos
is_drawing= True #Variable para controlar si se sigue dibujando

def printCoordinate(event, x, y, flags, params):
    global points, is_drawing
    if event == cv2.EVENT_LBUTTONDOWN and is_drawing:
        points.append((x, y))  # Almacena las coordenadas del punto en la lista
        cv2.circle(resized_image, (x, y), 3, (255, 255, 255), -1)
        strXY = '(' + str(x) + ',' + str(y) + ')'
        font = cv2.FONT_HERSHEY_PLAIN
        cv2.putText(resized_image, strXY, (x + 10, y - 10), font, 1, (255, 255, 255))
        cv2.imshow("image", resized_image)

        # Conecta todos los puntos existentes con líneas
        if len(points) > 1:
            for i in range(1, len(points)):
                cv2.line(resized_image, points[i - 1], points[i], (0, 0, 255), 5)
                cv2.imshow("image", resized_image)

image = cv2.imread('imagenes/23.jpg')  # imagen de prueba
# Reajustar el tamaño de la imagen
resized_image = cv2.resize(image, (900, 900))
cv2.imshow("image", resized_image)

cv2.setMouseCallback("image", printCoordinate)

while True:
    key = cv2.waitKey(0)  # Espera indefinidamente hasta que se presione una tecla
    if key == 13:  # 13 tecla "Enter"
        is_drawing = not is_drawing  # Cambia el estado de dibujo (iniciar/terminar)
        if not is_drawing:  # Verifica si el dibujo ha terminado
            # Verifica si la figura es cerrada
            if len(points) >= 3:  # Se necesitan al menos 3 puntos para formar una figura cerrada
                first_point = points[0]
                last_point = points[-1]
                threshold_distance = 10  # Umbral de distancia para considerar la figura cerrada

                # Calcula la distancia euclidiana entre el punto inicial y el punto final
                distance = np.sqrt((last_point[0] - first_point[0])**2 + (last_point[1] - first_point[1])**2)

                if distance < threshold_distance:
                    print("La figura es cerrada.")
                else:
                    print("La figura es abierta.")
    elif key == 27:  # Presiona la tecla Esc para salir
        break

cv2.destroyAllWindows()
