import cv2
import random
import numpy as np

img = cv2.imread('imagenes/18.jpg')

#rect = cv2.rectangle(img, (100, 100), (200, 200), (0, 0, 255), 2)

# Reajustar el tamaño de la imagen
resized_image = cv2.resize(img, (900, 900))

# Obtener el tamaño reajustado de la imagen
resized_height, resized_width, _ = resized_image.shape

# Generar coordenadas aleatorias para el rectángulo dentro del rango de la imagen reajustada
x1 = random.randint(0, resized_width)
y1 = random.randint(0, resized_height)
x2 = random.randint(0, resized_width)
y2 = random.randint(0, resized_height)

# Asegurarse de que sea un rectángulo válido
if x1 == x2:
    x2 = (x2 + 1) % resized_width
if y1 == y2:
    y2 = (y2 + 1) % resized_height

# Dibujar el rectángulo en la imagen reajustada
rect = resized_image.copy()
cv2.rectangle(rect, (x1, y1), (x2, y2), (0, 0, 255), 2)

gray = cv2.cvtColor(resized_image, cv2.COLOR_BGR2GRAY) 

mask = cv2.inRange(gray, 128, 255)

result = cv2.bitwise_and(rect, rect, mask=mask)

# Calcular el área del rectángulo
rect_width = abs(x2 - x1)
rect_height = abs(y2 - y1)
rect_area = rect_width * rect_height


# Crear una máscara dentro del área del rectángulo
mask_rect = np.zeros_like(gray)
mask_rect[y1:y2, x1:x2] = 255

# Aplicar la máscara a la imagen en escala de grises
dark_mask = cv2.bitwise_and(gray, gray, mask=mask_rect)

# Definir el rango de colores oscuros (en este caso, valores bajos de intensidad)
lower_bound = 0
upper_bound = 128

# Crear una máscara para resaltar el rango de colores oscuros
dark_highlighted_area = cv2.inRange(dark_mask, lower_bound, upper_bound)
 
# Crear una máscara invertida para los píxeles que no están en la región oscura
inverse_mask = cv2.bitwise_not(dark_highlighted_area)


# Aplicar la máscara invertida a la imagen original para obtener la región no destacada
non_dark_region = cv2.bitwise_and(resized_image, resized_image, mask=inverse_mask)


# conteo pixeles mitosis
#highlighted_pixel_count = cv2.countNonZero(dark_highlighted_area)
highlighted_pixel_count = cv2.countNonZero(dark_highlighted_area)

#Conteo No mitosis
#non_highlighted_pixel_count = (x2 - x1) * (y2 - y1) - highlighted_pixel_count
non_highlighted_pixel_count = rect_area - highlighted_pixel_count
non_highlighted_pixel_count = max(non_highlighted_pixel_count, 0)  # Asegurarse de que no sea negativo


print("Area del rectangulo:", rect_area)
print("Area mitosis:", highlighted_pixel_count) 
print("Area NO mitosis", non_highlighted_pixel_count)


# Mostrar la imagen reajustada con el rectángulo
#cv2.imshow('imagen original',resized_image)
cv2.imshow('Imagen original', result) 
cv2.imshow('no destacado', non_dark_region)
cv2.imshow('imagen destacada', dark_highlighted_area)
cv2.waitKey(0)
cv2.destroyAllWindows() 