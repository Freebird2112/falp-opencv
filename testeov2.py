import cv2
import random
import numpy as np

#LEER LA IMÁGEN
img = cv2.imread('imagenes/23.jpg')

# Reajuste tamaño imagen
resized_image = cv2.resize(img, (900, 900))

# Obtener el tamaño reajustado de la imagen
resized_height, resized_width, _ = resized_image.shape

# Definir el tamaño máximo del rectángulo
max_rect_size = min(resized_width, resized_height) // 2

# Generar coordenadas aleatorias para el punto superior izquierdo del rectángulo
x1 = random.randint(0, resized_width - max_rect_size)
y1 = random.randint(0, resized_height - max_rect_size)

# Generar ancho y alto aleatorios para el rectángulo
rect_width = random.randint(1, max_rect_size)
rect_height = random.randint(1, max_rect_size)

# Calcular las coordenadas del punto inferior derecho del rectángulo
x2 = x1 + rect_width
y2 = y1 + rect_height

# Dibujar el rectángulo en la imagen reajustada
cv2.rectangle(resized_image, (x1, y1), (x2, y2), (0, 0, 255), 2)

#CONVERTIR LA IMAGEN A ESCALA DE GRISES
gray = cv2.cvtColor(resized_image, cv2.COLOR_BGR2GRAY)


#####################################
##CONTEO CÉLULAS BLANCAS

# Aplicar un umbral para obtener una imagen binaria
_, thresh = cv2.threshold(gray, 128, 255, cv2.THRESH_BINARY)

# Encontrar los contornos en la imagen binaria
contours, _ = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

# Inicializar el contador de células dentro del rectángulo
cell_count = 0

# Recorrer cada contorno encontrado
for contour in contours:
  # Obtener las coordenadas del rectángulo que rodea al contorno
  x, y, w, h = cv2.boundingRect(contour)
  
  # Verificar si el contorno está completamente dentro del rectángulo original
  if x >= x1 and y >= y1 and x + w <= x2 and y + h <= y2:
    # Incrementar el contador de células
    cell_count += 1
    
    # Dibujar el contorno en la imagen reajustada
    cv2.drawContours(resized_image, [contour], -1, (0, 255, 0), 1)
    
    # Mostrar el número del contorno en la imagen reajustada
    cv2.putText(resized_image, str(cell_count), (x + w // 2, y + h // 2), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 1)

# Mostrar el contador de células en la consola
print(f"El numero de celulas dentro del rectangulo es: {cell_count}")
############################################


#CREACION MÁSCARA COLORES AZULES

hsv_image = cv2.cvtColor(resized_image, cv2.COLOR_BGR2HSV)

# Defino el rango de colores azules o celestes en HSV
lower_blue = np.array([90, 50, 50])  # Rango inferior
upper_blue = np.array([130, 255, 255])  # Rango superior

 #crear una máscara para resaltar los colores azules o celestes
blue_mask = cv2.inRange(hsv_image[y1:y2, x1:x2], lower_blue, upper_blue)

# Aplicar la máscara a la imagen original reajustada
blue_highlighted_area = np.zeros_like(resized_image)

# Asignar la máscara de color azul dentro del área del rectángulo
blue_highlighted_area[y1:y2, x1:x2] = cv2.merge([blue_mask, blue_mask, blue_mask])

# Aplicar la máscara a la imagen original reajustada
blue_highlighted_image = cv2.bitwise_and(resized_image, blue_highlighted_area)







##CONTEO CÉLULAS AZULES
# Convertir la imagen a HSV
hsv_image = cv2.cvtColor(resized_image, cv2.COLOR_BGR2HSV)

# Definir el rango de colores azules o celestes en HSV
lower_blue = np.array([80, 50, 50])  # Rango inferior
upper_blue = np.array([140, 255, 255])  # Rango superior

# Crear una máscara para resaltar los colores azules o celestes solo dentro del rectángulo
blue_mask = cv2.inRange(hsv_image[y1:y2, x1:x2], lower_blue, upper_blue)

# Encontrar los contornos en la máscara de color azul
blue_contours, _ = cv2.findContours(blue_mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

# Inicializar el contador de células azules dentro del rectángulo
blue_cell_count = 0



# Recorrer cada contorno encontrado en la máscara de color azul
for blue_contour in blue_contours:
  # Obtener las coordenadas del rectángulo que rodea al contorno
  x, y, w, h = cv2.boundingRect(blue_contour)
  
  # Verificar si el contorno está completamente dentro del rectángulo original
  if x >= x1 and y >= y1 and x + w <= x2 and y + h <= y2:
    # Incrementar el contador de células azules
    blue_cell_count += 1
    
    # Dibujar el contorno en la imagen reajustada
    cv2.drawContours(resized_image, [blue_contour], -1, (255, 0, 0), 1)
    
    # Mostrar el número del contorno en la imagen reajustada
    cv2.putText(resized_image, str(blue_cell_count), (x + w // 2, y + h // 2), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 1)

# Mostrar el contador de células azules en la consola
print(f"El numero de celulas azules dentro del rectangulo es: {blue_cell_count}")





# Crear una máscara dentro del área del rectángulo
mask_rect = np.zeros_like(gray)
mask_rect[y1:y2, x1:x2] = 255

# Aplicar la máscara a la imagen en escala de grises
dark_mask = cv2.bitwise_and(gray, gray, mask=mask_rect)

# Definir el rango de colores oscuros (en este caso, valores bajos de intensidad)
lower_bound = 0
upper_bound = 128

# Crear una máscara para resaltar el rango de colores oscuros
dark_highlighted_area = cv2.inRange(dark_mask, lower_bound, upper_bound)
# Crear una máscara para los píxeles NO destacados en el rango oscuro 
non_dark_highlighted_area = cv2.bitwise_not(dark_highlighted_area)





#CREACION MÁSCARA COLORES Blancos

# Definir el rango de colores blancos en HSV
lower_white = np.array([0, 0, 200])  # Rango inferior
upper_white = np.array([180, 50, 255])  # Rango superior

# Crear una máscara para resaltar los colores blancos solo dentro del rectángulo
white_mask = cv2.inRange(hsv_image[y1:y2, x1:x2], lower_white, upper_white)

# Crear una máscara del mismo tamaño que la imagen reajustada
white_highlighted_area = np.zeros_like(resized_image)

# Asignar la máscara de color blanco dentro del área del rectángulo
white_highlighted_area[y1:y2, x1:x2] = cv2.merge([white_mask, white_mask, white_mask])

# Aplicar las máscaras a la imagen original reajustada
white_highlighted_image = cv2.bitwise_and(resized_image, white_highlighted_area)


'''
##CONTEO CÉLULAS BLANCAS

# Aplicar un umbral para obtener una imagen binaria
_, thresh = cv2.threshold(gray, 128, 255, cv2.THRESH_BINARY)

# Encontrar los contornos en la imagen binaria
contours, _ = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

# Inicializar el contador de células dentro del rectángulo
cell_count = 0

# Recorrer cada contorno encontrado
for contour in contours:
  # Obtener las coordenadas del rectángulo que rodea al contorno
  x, y, w, h = cv2.boundingRect(contour)
  
  min_contour_area = 100  # Ajusta este valor según tus necesidades
  # Verificar si el contorno está completamente dentro del rectángulo original
  if x >= x1 and y >= y1 and x + w <= x2 and y + h <= y2 and cv2.contourArea(contour) >= min_contour_area:
    # Incrementar el contador de células
    cell_count += 1
    
    # Dibujar el contorno en la imagen reajustada
    cv2.drawContours(resized_image, [contour], -1, (0, 255, 0), 1)
    
    # Mostrar el número del contorno en la imagen reajustada
    cv2.putText(resized_image, str(cell_count), (x + w // 2, y + h // 2), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 1)

'''







# Calcular el área de los píxeles destacados(dentro del rectángulo)
area_pixeles_destacados = np.count_nonzero(dark_highlighted_area[y1:y2, x1:x2])

# Calcular el área de los píxeles que no están resaltados en non_dark_highlighted_area
area_no_destacada = np.count_nonzero(blue_highlighted_image[y1:y2, x1:x2])

indice_proliferacion = (area_pixeles_destacados / area_no_destacada)*100



# Mostrar la imagen reajustada con el rectángulo y el área resaltada de colores oscuros
#cv2.imshow('Dark Highlighted Area', dark_highlighted_area)
cv2.imshow('Resized Image with Rectangle', resized_image)
cv2.imshow('Celulas no marcadas(azules)', blue_highlighted_image)
cv2.imshow('White Area', white_highlighted_image)
#cv2.imshow('Blue cells', blue_mask)
cv2.imshow('Celulas marcadas', non_dark_highlighted_area)
cv2.waitKey(0)
cv2.destroyAllWindows()
